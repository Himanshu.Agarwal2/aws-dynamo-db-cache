# Name & Tagging
ally_application_id = "104154"
application_name    = "dynamodb"
service_name        = "poc"
owner               = "Deposits API"
data_classification = "Proprietary"
issrcl_level        = "Low"
scm_project         = ""
scm_repo            = ""
#dapi_kms_key = "alias/dapi-secrets-104154-dev-p-6"
#dapi_secret = "dapi-form-svc-secret"


awsacct_cidr_code = {
  default = "010-073-112-000"
  dev     = "010-073-112-000"
  qa      = "010-073-128-000"
  cap     = "010-073-128-000"
  psp     = "010-073-160-000"
  prod    = "010-073-160-000"
}

# image will have to be built and pointed to in a different manner but this is initial setup
tarball_path = "../../image.tar"
dynamo_db_table_name = "dynamopoc-table"
