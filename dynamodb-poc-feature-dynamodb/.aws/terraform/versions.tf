terraform {
  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    crane = {
      source  = "terraform-registry.services.ally.com/ally/crane"
      version = "1.1.0"
    }
    http = {
     source = "hashicorp/http"
    }
  }
}
