data "aws_caller_identity" "current" {}

#data "aws_kms_key" "dapi_kms" {
 # key_id = var.dapi_kms_key
#}
module "namespace" {
  source              = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-name-and-tags.git?ref=v1"
  application_name    = var.application_name
  service_name        = var.service_name
  workspace           = terraform.workspace
  environment         = var.environment
  application_id      = var.ally_application_id
  data_classification = var.data_classification
  issrcl_level        = var.issrcl_level
  owner               = var.owner
  scm_project         = var.scm_project
  scm_repo            = var.scm_repo
  scm_branch          = var.scm_branch
  scm_commit_id       = var.scm_commit_id
  additional_tags     = {
    "tf_starter" = var.creation_date
  }
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-state/browse
module "state" {
  source        = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-state.git?ref=v2"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-ecr/browse
module "ecr" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-ecr.git?ref=v1"
  namespace   = module.namespace.lower_resource_name
  name        = "dynamodb-poc"
  environment = var.environment
  tags        = module.namespace.tags
}

resource "crane_push" "image_push" {
  repository = module.ecr.repository_url
  tarball    = "../../image.tar"
  tag        = var.image_tag
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-private-ssl/browse
module "aws_dns" {
  source     = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-private-ssl.git?ref=v1"
  tags       = module.namespace.tags
  short_name = module.namespace.lower_resource_name
  route53_alias = {
    name                   = module.ecs_alb.alb_frontend.dns_name
    zone_id                = module.ecs_alb.alb_frontend.zone_id
    evaluate_target_health = false
  }
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-ecs-fargate/browse
module "app_container" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-ecs-fargate.git//modules/container-definition?ref=v5"
  namespace       = module.namespace.lower_resource_name
  region          = var.region
  container_name  = "dynamodb-poc"
  container_image = "${module.ecr.repository_name}:${crane_push.image_push.tag}"
  container_cpu   = 300
  container_memory = {
    allowed  = 2048
    reserved = 2048
  }
  port_mappings = [
    {
      containerPort = 8443
      protocol      = "tcp"
    }
  ]
  #secrets = [
      #{
          #name =  "resource-id-key"
          #valueFrom = module.dapi_form_svc_secret.secret.arn
          #valueFrom: arn:aws:secretsmanager:us-east-1:650487181597:secret:dapi-secret-cQOeIm
      #}
      #]
  mount_points = [
    {
      sourceVolume  = "certificates",
      containerPath = "/etc/ally/ssl",
      readOnly      = false
    }
  ]
  container_depends_on = [
    {
      containerName = module.ssl_init_container.definition.name
      condition     = "SUCCESS"
    }
  ]
}

module "ssl_init_container" {
  source                  = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-ecs-fargate.git//modules/ssl-init-container-definition?ref=v5"
  namespace               = module.namespace.lower_resource_name
  region                  = var.region
  private_certificate_arn = module.aws_dns.certificate.arn
}
#module "dapi_form_svc_secret" {
  #source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-secretsmanager.git?ref=v1"
  #description = "Dapi form service secret"
  #environment = var.environment
  #kms_key_arn = data.aws_kms_key.dapi_kms.arn
  #name        = var.dapi_secret
  #namespace   = module.namespace.lower_resource_name
  #tags        = module.namespace.tags
#}
module "task" {
  source    = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-ecs-fargate.git//modules/task-definition?ref=v5"
  name      = "td"
  namespace = module.namespace.lower_resource_name
  tags      = module.namespace.tags

  task_execution_role  = module.ecs.task_execution_role
  container_definition = [
    module.app_container,
    module.ssl_init_container
  ]
  volumes = [
    {
      name = "certificates"
    }
  ]
}

module "ecs" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-ecs-fargate.git?ref=v5"
  name        = "services"
  namespace   = module.namespace.lower_resource_name
  environment = var.environment
  tags        = module.namespace.tags
  container_definition = [
    module.app_container,
    module.ssl_init_container,
  ]
}

module "ecs_service" {
  source          = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-ecs-fargate.git//modules/service?ref=v5"
  name            = "svc"
  namespace       = module.namespace.lower_resource_name
  tags            = module.namespace.tags
  vpc_cidr_code   = lookup(var.awsacct_cidr_code, var.environment)
  ecs_cluster     = module.ecs.cluster
  task_definition = module.task.definition
  lb_targets      = module.ecs_alb.lb_targets
}

module "ecs_alb" {
  source              = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-ecs-fargate.git//modules/load-balancer?ref=v5"
  type                = "alb"
  name                = "lb"
  namespace           = module.namespace.lower_resource_name
  tags                = module.namespace.tags
  vpc_cidr_code       = lookup(var.awsacct_cidr_code, var.environment)
  internal            = true
  tls_certificate_arn = module.aws_dns.certificate.arn
  container_name = module.app_container.definition.name
  container_port = module.app_container.definition.portMappings[0].containerPort

  alb_health_check = {
    allowed_response_codes = "200-499"
    path                   = "/"
  }
}

################### Cloudwatch Dashboard setup
 
resource "aws_cloudwatch_dashboard" "main" {
  dashboard_name = "${module.ecs.cluster.name}-Alert-Trigger"
  dashboard_body = <<EOF
  {
    "start": "-PT24H",
    "periodOverride": "inherit",
    "widgets": [
        {
            "type": "metric",
            "x": 0,
            "y": 24,
            "width": 12,
            "height": 12,
            "properties": {
                "metrics": [
                    [ "ECS/ContainerInsights", "TaskSetCount", "ServiceName", "${module.ecs_service.service.name}", "ClusterName", "${module.ecs.cluster.name}" ],
                    [ ".", "PendingTaskCount", ".", ".", ".", "." ],
                    [ ".", "RunningTaskCount", ".", ".", ".", "." ],
                    [ ".", "DeploymentCount", ".", ".", ".", "." ],
                    [ ".", "DesiredTaskCount", ".", ".", ".", "." ]
                ],
                "view": "timeSeries",
                "stacked": true,
                "region": "us-east-1",
                "period": 300,
                "title": "Task-status"
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 0,
            "width": 12,
            "height": 12,
            "properties": {
                "metrics": [
                    [ "ECS/ContainerInsights", "TaskSetCount", "ServiceName", "${module.ecs_service.service.name}", "ClusterName", "${module.ecs.cluster.name}", { "visible": false } ],
                    [ ".", "CpuReserved", ".", ".", ".", "." ],
                    [ ".", "CpuUtilized", ".", ".", ".", "." ],
                    [ ".", "MemoryReserved", ".", ".", ".", "." ],
                    [ ".", "MemoryUtilized", ".", ".", ".", "." ],
                    [ ".", "NetworkTxBytes", ".", ".", ".", "." ],
                    [ ".", "NetworkRxBytes", ".", ".", ".", "." ],
                    [ ".", "StorageReadBytes", ".", ".", ".", "." ],
                    [ ".", "StorageWriteBytes", ".", ".", ".", "." ],
                    [ ".", "EphemeralStorageReserved", ".", ".", ".", "." ]
                ],
                "stacked": true,
                "region": "us-east-1",
                "stat": "Average",
                "period": 300,
                "title": "CPU-Memory-Storage"
            }
        }
     ]
  }
  EOF
}
module "dynamodb" {
  source    = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-dynamodb.git?ref=v3"
  namespace = module.namespace.lower_short_name
  name      = var.dynamo_db_table_name
  tags      = module.namespace.tags

 hash_key = {
    name = "UserId"
    type = "S"
  }
}
