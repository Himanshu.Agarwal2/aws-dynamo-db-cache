variable "region" {
  type        = string
  description = "The target AWS region to deploy to."
  default     = "us-east-1"
}

variable "environment" {
  type        = string
  description = "Env to use for resolving variables"
  default     = "default"
}

#variable "dapi_secret" {
  #type        = string
  #description = "Secert for dapi form svc"
  #default     = "default"
#}
#variable "dapi_kms_key" {
  #type        = string
  #description = "KMS key alias to encrypt secret"
  #default     = "default"
#}
variable "ally_application_id" {
  type        = string
  description = "The Ally Application ID of the project."
}

variable "application_name" {
  type        = string
  description = "The Ally Application Name of the project."
}

variable "service_name" {
  type        = string
  description = "The Ally Application service name."
}

variable "data_classification" {
  type        = string
  description = "The Ally data classification."
}

variable "issrcl_level" {
  type        = string
  description = "This tag is used to identify the Information System Security Risk Classification Level for the resource with one of the following: low, medium, high. See the readme for the document to determine this."
}

variable "owner" {
  type        = string
  description = "Team owner of the application"
}

variable "scm_project" {
  type        = string
  description = "The Bitbucket project used to deploy the resource."
}

variable "scm_repo" {
  type        = string
  description = "The Bitbucket repository used to deploy the release."
}

variable "scm_branch" {
  type        = string
  description = "The Bitbucket and tag used to deploy the resource."
  default     = "local-deploy"
}

variable "scm_commit_id" {
  type        = string
  description = "The specific commit that was used to deploy the resource."
  default     = "000000"
}

variable "creation_date" {
  type        = string
  description = "The date and time this project was generated."
  default     = "2021-12-09T21:02:39.419098"
}

variable "awsacct_cidr_code" {
  type        = map
  description = "The CIDR block assigned to the account VPC"
}

variable "tarball_path" {
  type        = string
  description = "Path to the image tarball to push to ECR"
}

variable "image_tag" {
  type        = string
  description = "The container image tag"
}

variable "health_check_grace_period_seconds" {
  type = number
  default = 60
}
variable "dynamo_db_table_name" {
  type = string
  description = "Name of DynamoDb Table"
}

# variable "alb_health_check_extra_options" {
#   description = "Extra fine-tune configuration options for the Target Group health checks."
#   type = object({
#     interval            = number
#     timeout             = number
#     healthy_threshold   = number
#     unhealthy_threshold = number
#   })
#   default = {
#     interval            = 100
#     timeout             = 5
#     healthy_threshold   = 3
#     unhealthy_threshold = 3
#   }
# }
