output "container_image" {
  description = "The full image URL including registry, repo, and tag"
  value       = "${module.ecr.repository_url}:${var.image_tag}"
}

output "environment_url" {
  description = "Private DNS URL. This value is picked up by Gitlab for the environment URL"
  value       = "https://${module.aws_dns.record.fqdn}"
}