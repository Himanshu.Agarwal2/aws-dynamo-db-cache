import boto3
import requests
import getpass
import configparser
import base64
import re
import xml.etree.ElementTree as ET
import os
import sys
from bs4 import BeautifulSoup
from os.path import expanduser
from urllib.parse import urlparse, urlunparse
import getopt

class saml:
    def __init__(self, username, password, profile, duration):
        self.region = "us-east-1"
        self.outputformat = "json"
        self.awsconfigfile = "/.aws/credentials"
        self.sslverification = True
        self.idpentryurl = 'https://adfsp.ally.com/adfs/ls/IdpInitiatedSignOn.aspx?loginToRp=urn:amazon:webservices'

        self.username = username
        self.password = password

        self.token = {}

        # start initial requests session
        self.session = requests.Session()

        self.profile = profile
        self.duration = duration

    def __loginForm(self):
        # Programmatically get the SAML assertion
        # Opens the initial IdP url and follows all of the HTTP302 redirects, and
        # gets the resulting login page
        formresponse = self.session.get(self.idpentryurl, verify=self.sslverification)
        # Capture the idpauthformsubmiturl, which is the final url after all the 302s
        idpauthformsubmiturl = formresponse.url

        # Parse the response and extract all the necessary values
        # in order to build a dictionary of all of the form values the IdP expects
        formsoup = BeautifulSoup(formresponse.content, "html.parser")
        payload = {}

        for inputtag in formsoup.find_all(re.compile('(INPUT|input)')):
            name = inputtag.get('name','')
            value = inputtag.get('value','')
            if "user" in name.lower():
                #Make an educated guess that this is the right field for the username
                payload[name] = self.username
            elif "email" in name.lower():
                #Some IdPs also label the username field as 'email'
                payload[name] = self.username
            elif "pass" in name.lower():
                #Make an educated guess that this is the right field for the password
                payload[name] = self.password
            else:
                #Simply populate the parameter with the existing value (picks up hidden fields in the login form)
                payload[name] = value

        # Set our AuthMethod to Form-based auth because the code above sees two values
        # for authMethod and the last one is wrong
        payload['AuthMethod'] = 'FormsAuthentication'

        # Some IdPs don't explicitly set a form action, but if one is set we should
        # build the idpauthformsubmiturl by combining the scheme and hostname 
        # from the entry url with the form action target
        # If the action tag doesn't exist, we just stick with the 
        # idpauthformsubmiturl above
        for inputtag in formsoup.find_all(re.compile('(FORM|form)')):
            action = inputtag.get('action')
            loginid = inputtag.get('id')
            if (action and loginid == "loginForm"):
                parsedurl = urlparse(self.idpentryurl)
                idpauthformsubmiturl = parsedurl.scheme + "://" + parsedurl.netloc + action

        # Performs the submission of the IdP login form with the above post data
        loginresponse = self.session.post(idpauthformsubmiturl, data=payload, verify=self.sslverification)

        return loginresponse

    def __sendMFAPost(self, soup, url, payloadappend):
        payload = {}
        for inputtag in soup.find_all(re.compile('(INPUT|input)')):
            name = inputtag.get('name','')
            value = inputtag.get('value','')
            payload[name] = value
        response = self.session.post(url, data={**payload, **payloadappend}, verify=self.sslverification)
        return response

    def __getSelection(self, response):
        soup = BeautifulSoup(response.content, "html.parser")
        try:
            instructions = soup.find("p", { "id" : "instructions" }).get_text()
        except:
            instructions = ""

        if 'mobile' in instructions.lower():
            selection = "mobile"
        elif 'text' in instructions.lower():
            selection = "text"
        elif 'phone' in instructions.lower():
            selection = "phone"
        else:
            selection = ""
        return selection

    def __mfa(self, loginresponse):
        # MFA Step 1 - If you have MFA Enabled, there are two additional steps to authenticate
        # Capture the idpauthformsubmiturl, which is the final url after all the 302s
        mfaurl = loginresponse.url
        loginsoup = BeautifulSoup(loginresponse.text, "html.parser")
        # If I have more than one MFA Authenticaiton option, create list... Mobile App and Text\Phone.
        mfaoptions = [] 
        for mfatag in loginsoup.find_all(re.compile("^a")):
            mfatagtext = mfatag.get_text()
            if len(mfatagtext) >= 6:
                mfaoptions.append(mfatagtext)
        # If I have more than one MFA Authenticaiton option, update array... Mobile App and Text\Phone.     
        for mfaoption in mfaoptions:
            index = mfaoptions.index(mfaoption)
            mfaoptions.insert(index, mfaoption)
            mfaoptions.remove(mfaoption)
        # If I have more than one MFA Authenticaiton option, ask the user which one they want,
        # otherwise just proceed
        selectedmfaoption = 0
        verificationOption = ""

        if len(mfaoptions) > 1:
            i = 0
            print("Please choose your preferred MFA Authentication method:")
            for mfaoption in mfaoptions:
                print('[', i, ']: ', mfaoption)
                i += 1
            selectedmfaoption = input("Selection: ")
            verificationOption = "verificationOption" + selectedmfaoption
            
        print("Please check your MFA device")
        mfaresponse = self.__sendMFAPost(loginsoup,mfaurl,{'__EVENTTARGET':verificationOption,'AuthMethod':'AzureMfaServerAuthentication'})

        # MFA Step 2 - Fire\Send the form and wait for verification
        if len(mfaoptions) > 1:
            mfasoup = BeautifulSoup(mfaresponse.text, "html.parser")
            mfaresponse = self.__sendMFAPost(mfasoup,mfaurl,{'AuthMethod':'AzureMfaServerAuthentication'})

        ## figure out what selection they chose
        selection = self.__getSelection(mfaresponse)
        
        #MFA Step 3 - Phone or Text verification
        if selection == "text":
            mfasoup2 = BeautifulSoup(mfaresponse.content, "html.parser")
            PasscodeInput = input("MFA verification code:")
            mfaresponse = self.__sendMFAPost(mfasoup2,mfaurl,{'OneTimePasscode':PasscodeInput,'AuthMethod':'AzureMfaServerAuthentication'})
        return mfaresponse

    def __validateResponse(self, response):
        assertion = ''
        soup = BeautifulSoup(response.text, "html.parser")
        for inputtag in soup.find_all('input'):
            if(inputtag.get('name') == 'SAMLResponse'):
                assertion = inputtag.get('value')
        if assertion == '':
            return False
        return assertion

    def __getRolesToAssume(self, response):
        # Overwrite and delete the credential variables, just for safety
        self.username = '##############################################'
        self.password = '##############################################'
        del self.username
        del self.password

        assertion = self.__validateResponse(response)
        if (assertion == False):
            try:
                mfa_response = self.__mfa(response)
                assertion = self.__validateResponse(mfa_response)
                if not assertion:
                    print("Verification took to long. Try again.")
                    sys.exit(2)
            except:
                print("Invalid SAML response")
                sys.exit(2)
        # Parse the returned assertion and extract the authorized roles
        awsroles = []
        root = ET.fromstring(base64.b64decode(assertion))
        for saml2attribute in root.iter('{urn:oasis:names:tc:SAML:2.0:assertion}Attribute'):
            if (saml2attribute.get('Name') == 'https://aws.amazon.com/SAML/Attributes/Role'):
                for saml2attributevalue in saml2attribute.iter('{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue'):
                    awsroles.append(saml2attributevalue.text)

        # Note the format of the attribute value should be role_arn,principal_arn
        # but lots of blogs list it as principal_arn,role_arn so let's reverse
        # them if needed
        for awsrole in awsroles:
            chunks = awsrole.split(',')
            if'saml-provider' in chunks[0]:
                newawsrole = chunks[1] + ',' + chunks[0]
                index = awsroles.index(awsrole)
                awsroles.insert(index, newawsrole)
                awsroles.remove(awsrole)

        # If I have more than one role, ask the user which one they want,
        # otherwise just proceed
        print("")
        if len(awsroles) > 1:
            i = 0
            print("Please choose the role you would like to assume:")
            for awsrole in awsroles:
                print('[', i, ']: ', awsrole.split(',')[0])
                i += 1
            selectedroleindex = input("Selection: ")

            # Basic sanity check of input
            if int(selectedroleindex) > (len(awsroles) - 1):
                print('You selected an invalid role index, please try again')
                sys.exit(0)

            role_arn = awsroles[int(selectedroleindex)].split(',')[0]
            principal_arn = awsroles[int(selectedroleindex)].split(',')[1]
        else:
            role_arn = awsroles[0].split(',')[0]
            principal_arn = awsroles[0].split(',')[1]

        # Use the assertion to get an AWS STS token using Assume Role with SAML
        sts_client = boto3.client('sts', region_name=self.region, verify=False)
        self.token = sts_client.assume_role_with_saml(RoleArn=role_arn, PrincipalArn=principal_arn, SAMLAssertion=assertion, DurationSeconds=self.duration)

    def __setCredentialFile(self):
        # Toss the STS credentials into the aws config file
        home = expanduser("~")
        filename = home + self.awsconfigfile

        # make .aws dir if it does not exist
        try:
            os.mkdir("{}/.aws".format(home))
        except FileExistsError:
            print("Credential folder already exists; skipping")

        # Create credentials file if it doesn't exist
        open(filename, 'a').close()

        # read credential file
        config = configparser.RawConfigParser()
        config.read(filename)

        # create the config section if it doesn't already exist
        if not config.has_section(self.profile):
            config.add_section(self.profile)

        config.set(self.profile, 'output', self.outputformat)
        config.set(self.profile, 'region', self.region)
        config.set(self.profile, 'aws_access_key_id', self.token['Credentials']['AccessKeyId'])
        config.set(self.profile, 'aws_secret_access_key', self.token['Credentials']['SecretAccessKey'])
        config.set(self.profile, 'aws_session_token', self.token['Credentials']['SessionToken'])

        # # Write the updated config file
        with open(filename, 'w+') as configfile:
            config.write(configfile)

        print('\n\n----------------------------------------------------------------')
        print('Your new access key pair has been stored in the AWS configuration file {} under the {} profile.'.format(filename,self.profile))
        print('Note that it will expire at {}.'.format(self.token['Credentials']['Expiration']))
        print('After this time, you may safely rerun this script to refresh your access key pair.')
        print('To use this credential, call the AWS CLI with the --profile option (e.g. aws --profile {} ec2 describe-instances).'.format(self.profile))
        print('----------------------------------------------------------------\n\n')   

    def __checkForError(self, response):
        soup = BeautifulSoup(response.text, "html.parser")
        error = soup.find("label", { "id" : "errorText" })
        if error != None:
            print(error.get_text())
            sys.exit(2)

    def authenticate(self):
        # call the login form and get the response
        response = self.__loginForm()
        self.__checkForError(response)
        self.__getRolesToAssume(response)
        self.__setCredentialFile()

    def getToken(self):
        if 'Credentials' in self.token:
            print("\n\nAccessKey: {}\nSecretAccessKey: {}\nSessionToken: {}".format(self.token['Credentials']['AccessKeyId'],self.token['Credentials']['SecretAccessKey'],self.token['Credentials']['SessionToken']))
        return self.token

#############################################################################  
def main(argv):
    ## defaults
    profile = "saml"
    duration = 3600
    username = None
    password = None

    try:
        opts, args = getopt.getopt(argv,"h:",["profile=","duration=","user=","password="])
    except getopt.GetoptError:
        print('main.py --profile <profile> --duration <duration_seconds>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('main.py --profile <profile> --duration <duration_seconds>')
            sys.exit()
        elif opt in ("--profile"):
            if arg == '' or arg == None:
                print("Profile can not be blank")
                sys.exit(2)
            profile = str(arg)
        elif opt in ("--duration"):
            if int(arg) < 900:
                print("You must specify a duration of at least 900 seconds")
                sys.exit(2)
            duration = int(arg)

        ## Only use these for automation pipeline purposes may want to just ingest env vars
        elif opt in ("--user"):
            username = str(arg)
        elif opt in ("--password"):
            password = str(arg)

    # ingest env variables if they are set
    if "saml_username" in os.environ and "saml_password" in os.environ:
        username = os.environ["saml_username"]
        password = os.environ["saml_password"]

    if not username or not password:
        username = input("Username: ")
        password = getpass.getpass()

    main = saml(username,password, profile, duration)
    main.authenticate()

    # Uncomment this to print credentials in console
    #main.getToken()

#####
main(sys.argv[1:])
