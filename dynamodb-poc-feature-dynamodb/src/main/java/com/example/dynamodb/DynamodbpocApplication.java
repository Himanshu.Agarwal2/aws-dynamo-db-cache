package com.example.dynamodb;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
//import org.apache.camel.support.SimpleRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;

@SpringBootApplication
public class DynamodbpocApplication {
	
	@Bean(name="client")
	public static AmazonDynamoDB client(){
		/*ProfileCredentialsProvider credentialProvider = new ProfileCredentialsProvider("/home/gitpod/.aws/credentials",
	            "saml");
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
	            .withCredentials(credentialProvider).withRegion(Regions.US_EAST_1)
	            .build();*/
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		return client;
		
	}

	public static void main(String[] args) throws Exception {
		DynamoDBRoute dynamoDBRoute = new DynamoDBRoute();
		SpringApplication.run(DynamodbpocApplication.class, args);
		/*ProfileCredentialsProvider credentialProvider = new ProfileCredentialsProvider("/home/gitpod/.aws/credentials",
	            "saml");
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
	            .withCredentials(credentialProvider).withRegion(Regions.US_EAST_1)
	            .build();*/
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		SimpleRegistry reg = new SimpleRegistry();
		reg.put("client", client);
        
		CamelContext context =  new DefaultCamelContext(reg);
		context.addRoutes(new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
//				from("timer://runOnce?repeatCount=1").bean(UserDetailsRepositoryV2.class, "addUserDetails").log("inside main route");
				from("timer://runOnce?repeatCount=1").to("aws-ddb://user?amazonDDBClient=#client&operation=Scan").log("${header.CamelAwsDdbItems}").
				to("direct:insertRecordToDB");
				
			}
		});
		context.addRoutes(dynamoDBRoute);
		context.start();
		Thread.sleep(10000);
		context.stop();
	}

}



