package com.example.dynamodb;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

@Component
public class DynamoDBRoute extends RouteBuilder{

	 @Override
	  public void configure() throws Exception {
			Map<String, AttributeValue> itemAttributesMap = new HashMap<String, AttributeValue>();
			Map<String, AttributeValue> addressMap = new HashMap<String, AttributeValue>();
			AttributeValue addressValue = new AttributeValue();
			addressMap.put("pin", new AttributeValue("411051"));
			addressMap.put("city", new AttributeValue("Pune"));
			
			addressValue.setM(addressMap);
			
			UserDetails userDetail = new UserDetails("firstName8", "lastName8");
			UUID uuid = UUID.randomUUID();
			itemAttributesMap.put("user_id", new AttributeValue(uuid.toString()));
			itemAttributesMap.put("first_name", new AttributeValue(userDetail.getFirstName()));
			itemAttributesMap.put("last_name", new AttributeValue(userDetail.getLastName()));
			itemAttributesMap.put("address", addressValue);
			
		 from("direct://insertRecordToDB").setHeader("CamelAwsDdbItem", constant(itemAttributesMap)).log("record being inserted is").log("${header.CamelAwsDdbItem}")
		 .to("aws-ddb://user?amazonDDBClient=#client&operation=PutItem").log("${header.CamelAwsDdbItems}");
	 }
}
