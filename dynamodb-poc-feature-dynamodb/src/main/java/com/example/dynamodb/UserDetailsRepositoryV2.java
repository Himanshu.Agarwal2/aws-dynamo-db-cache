package com.example.dynamodb;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * @author md
 *
 */
//@Repository
public class UserDetailsRepositoryV2 {
	private static Logger LOGGER = LoggerFactory.getLogger(UserDetailsRepositoryV2.class);
	
	/*ProfileCredentialsProvider credentialProvider = new ProfileCredentialsProvider("/home/gitpod/.aws/credentials",
            "saml");
	
	AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
            .withCredentials(credentialProvider).withRegion(Regions.US_EAST_1)
            .build();*/
	AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();

	DynamoDB dynamoDb = new DynamoDB(client);

	public UserDetails getUserDetails(String key) {
		// table name
		Table table = dynamoDb.getTable("user");
		GetItemSpec spec = new GetItemSpec().withPrimaryKey("user_id", key);
		try {
			System.out.println("Attempting to read item");
			Item outcome = table.getItem(spec);
			if (Objects.nonNull(outcome)) {
				UserDetails user = new UserDetails();
				user.setUserId(outcome.get("user_id").toString());
				user.setFirstName(outcome.get("first_name").toString());
				user.setLastName(outcome.get("last_name").toString());
				return user;
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred during getUserDetails : ", e);
		}
		return null;
	}

	public String addUserDetails() {
		System.out.println("Inside addUserDetails method");
		UserDetails userDetail = new UserDetails();
		userDetail.setUserId("12");
		userDetail.setLastName("last1");
		userDetail.setFirstName("first1");
		
		Table table = dynamoDb.getTable("user");
		try {
			final Map<String, String> addressMap = new HashMap<String, String>();
			addressMap.put("city", "Pune");
			addressMap.put("pin", "411051");
			PutItemOutcome outcome = table.putItem(
					new Item().withPrimaryKey("user_id", userDetail.getUserId()).with("first_name", userDetail.getFirstName())
							.with("last_name", userDetail.getLastName()).withMap("address", addressMap));
			if (Objects.nonNull(outcome))
				return "SUCCESS";
			else
				return "FAILURE";
		} catch (Exception e) {
			LOGGER.error("Exception occurred while adding record to the db : ", e);
			return null;
		}
	}

}